import "./App.css";
import Counter from "./features/counter/Counter";
import PostLists from "./features/post/PostLists";
import AddPostForm from "./features/post/AddPostForm";

function App() {
  return (
    <div className="App m-2 .bg-light">
      <AddPostForm />
      <PostLists />
    </div>
  );
}

export default App;
