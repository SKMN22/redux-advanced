import { useDispatch } from "react-redux";
import { reactionAdded } from "./postSlice";

const reactions = {
  thumbsUp: "thumbsUp",
  wow: "wow",
  heart: "heart",
  rocket: "rocket",
  coffe: "coffe",
};

const ReactionButton = ({ post }) => {
  const dispatch = useDispatch();
  const reactionButtons = Object.entries(reactions).map(([name, value]) => {
    return (
      <button
        key={name}
        type="button"
        className="btn btn-secondary m-1"
        onClick={() => {
          dispatch(reactionAdded({ postId: post.id, reaction: name }));
        }}
      >
        {value + "  "}
        {post.reactions[name]}
      </button>
    );
  });
  return <div>{reactionButtons}</div>;
};

export default ReactionButton;
