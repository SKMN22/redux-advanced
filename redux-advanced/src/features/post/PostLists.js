import { useSelector, useDispatch } from "react-redux";
import {
  selectAllPosts,
  getPostStatus,
  getPostError,
  fetchPosts,
} from "./postSlice";
import { useEffect } from "react";

import React from "react";
import PostsExcerpt from "./PostsExcerpt";

const PostLists = () => {
  const dispatch = useDispatch();
  const post = useSelector(selectAllPosts);
  const postStatus = useSelector(getPostStatus);
  const postError = useSelector(getPostError);

  useEffect(() => {
    if (postStatus === "idle") {
      dispatch(fetchPosts());
    }
  }, [postStatus, dispatch]);

  useEffect(() => {
    
  },[]);

  let content;
  if (postStatus === "loading") {
    content = <p>"Loading ... "</p>;
  } else if (postStatus === "succeded") {
    const orderedPosts = post
      .slice()
      .sort((a, b) => b.date.localeCompare(a.date));
    content = orderedPosts.map((post) => (
      <PostsExcerpt key={post.id} post={post} />
    ));
  } else if (postStatus === "failed") {
    content = <p>{postError}</p>;
  }

  return (
    <section>
      <h2>Post</h2>
      {content}
    </section>
  );
};

export default PostLists;
