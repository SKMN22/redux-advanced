import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { nanoid } from "@reduxjs/toolkit";
import { sub } from "date-fns";
import axios from "axios";

const POST_URL = "https://jsonplaceholder.typicode.com/posts";

// const initialState = [
//   {
//     id: 1,
//     title: "Title 1",
//     content: "Content",
//     date: sub(new Date(), { minutes: 10 }).toISOString(),
//     reactions: {
//       thumbsUp: 0,
//       wow: 0,
//       heart: 0,
//       rocket: 0,
//       coffe: 0,
//     },
//   },
//   {
//     id: 2,
//     title: "Title 2",
//     content: "Content 2",
//     date: sub(new Date(), { minutes: 5 }).toISOString(),
//     reactions: {
//       thumbsUp: 0,
//       wow: 0,
//       heart: 0,
//       rocket: 0,
//       coffe: 0,
//     },
//   },
// ];

const initialState = {
  posts: [],
  status: "idle",
  error: null,
};

export const fetchPosts = createAsyncThunk("posts/fetchPosts", async () => {
  const response = await axios.get(POST_URL);
  return response.data;
});

export const addNewPost = createAsyncThunk(
  "posts/addNewPost",
  async (initialPost) => {
    try {
      const response = await axios.post(POST_URL, initialPost);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  }
);

// createSlic does not mutate state, it uses immerjs for "changing" object, so it is ok to use mutation state
// const postSlice = createSlice({
//   name: "posts",
//   initialState,
//   reducers: {
//     postAdded: {
//       reducer(state, action) {
//         state.push(action.payload);
//       },
//       prepare(title, content, userId) {
//         return {
//           payload: {
//             id: nanoid(),
//             title,
//             content,
//             date: new Date().toISOString(),
//             userId,
//             reactions: {
//               thumbsUp: 0,
//               wow: 0,
//               heart: 0,
//               rocket: 0,
//               coffe: 0,
//             },
//           },
//         };
//       },
//       reactionAdded(state, action) {
//         const { postId, reaction } = action.payload;
//         const existingPost = state.find((post) => post.id === postId);
//         if (existingPost) {
//           existingPost.reaction[reaction]++;
//         }
//       },
//     },
//   },
// });

const postSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    postAdded: {
      reducer(state, action) {
        state.push(action.payload);
      },
    },
    reactionAdded(state, action) {
      const { postId, reaction } = action.payload;
      const existingPost = state.posts.find((post) => post.id === postId);
      if (existingPost) {
        existingPost.reactions[reaction]++;
      }
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchPosts.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.status = "succeded";
        let min = 1;
        let loadedPosts = action.payload.map((post) => {
          post.date = sub(new Date(), { minutes: min++ }).toISOString();
          post.reactions = {
            thumbsUp: 0,
            wow: 0,
            heart: 0,
            rocket: 0,
            coffe: 0,
          };
          return post;
        });
        const localData = JSON.parse(localStorage.getItem("posts"));
        if (!localData) {
          localStorage.setItem("posts", JSON.stringify(loadedPosts));
        } else {
          loadedPosts = localData;
        }
        state.posts = state.posts.concat(loadedPosts);
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      })
      .addCase(addNewPost.fulfilled, (state, action) => {
        action.payload.userId = Number(action.payload.userId);
        action.payload.date = new Date().toISOString();
        action.payload.reactions = {
          thumbsUp: 0,
          wow: 0,
          heart: 0,
          rocket: 0,
          coffe: 0,
        };
        let data = JSON.parse(localStorage.getItem("posts"));
        action.payload.id = data.length + 1;
        console.log(action.payload);
        if (data) {
          console.log(data);
          data.push(action.payload);
          localStorage.setItem("posts", JSON.stringify(data));
        }
        state.posts.push(action.payload);
      });
  },
});

export const selectAllPosts = (state) => state.posts.posts;
export const getPostStatus = (state) => state.posts.status;
export const getPostError = (state) => state.posts.error;

export const { postAdded, reactionAdded } = postSlice.actions;

export default postSlice.reducer;
