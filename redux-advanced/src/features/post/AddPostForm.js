import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addNewPost } from "./postSlice";
import { selectAllUsers } from "../users/usersSlice";
import React from "react";

const AddPostForm = () => {
  const [title, setTitle] = useState("");
  const [content, setContent] = useState("");
  const [userId, setUserId] = useState("");
  const [addRequestStatus, setRequestStatus] = useState("idle");

  const users = useSelector(selectAllUsers);

  const onTitleChange = (e) => setTitle(e.target.value);
  const onContentChange = (e) => setContent(e.target.value);
  const onAuthorChange = (e) => setUserId(e.target.value);
  const dispatch = useDispatch();

  const canSave =
    [title, content, userId].every(Boolean) && addRequestStatus === "idle";

  const onSavePostClicked = () => {
    if (canSave) {
      try {
        setRequestStatus("pending");
        dispatch(addNewPost({ title, body: content, userId })).unwrap();
        setTitle("");
        setContent("");
        setUserId("");
      } catch (err) {
        console.error(err);
      } finally {
        setRequestStatus("idle");
      }
    }
  };

  const userOptions = users.map((user) => (
    <option key={user.id} value={user.id}>
      {user.name}
    </option>
  ));

  return (
    <section>
      <h2 className="bg-primary text-white">Add a New Post</h2>
      <h3 className="bg-secondary text-white">Delete after 2 mins</h3>
      <form>
        <div className="form-group">
          <label htmlFor="postTitle">Post Title: </label>
          <input
            type="text"
            id="postTitle"
            name="postTitle"
            value={title}
            onChange={onTitleChange}
          ></input>
        </div>
        <div className="form-group">
          <label htmlFor="postAuthor">Author: </label>
          <select id="postAuthor" value={userId} onChange={onAuthorChange}>
            <option value=""></option>
            {userOptions}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="postContent">Post Content: </label>
          <textarea
            id="postContent"
            name="postContent"
            value={content}
            onChange={onContentChange}
          ></textarea>
        </div>
        <button
          type="button"
          className="btn btn-primary"
          onClick={onSavePostClicked}
          disabled={!canSave}
        >
          Save
        </button>
      </form>
    </section>
  );
};

export default AddPostForm;
